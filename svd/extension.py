#!/usr/bin/env python
# -*- coding: utf-8 -*-

# MIT License
#
# Copyright (c) 2018 BayLibre
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import lxml.objectify

from svd.svd import SVDText

class SVDExtension(SVDText):
    """
        A class to load SVD file extension.

        SVD file extensions are incomplete SVD file made to be merged with
        the a regular SVD.
        This is required as SVD files may not include registers definitions
        for third party IP.

        This provides method to load, configure and merge SVD file extension
        with the main SVD file.
    """
    def configure_peripherals(self, properties):
        """
            Configure some properties of peripherals

            In order to have a SVD file extension that could be used to any
            SoC, some elements may be configured to 'NaN', which must be
            replaced by a valid for the SoC using it.
            This go through the peripherals and it properties, and replace
            the value defined in SVD to the one defined in 'properties'
            argument.

            Properties is expected to be in this format:
            properties = {
                '<peripheral_name>': {'baseAddress': '<address>'}
            }
            Currently, only 'baseAddress', 'dim' and 'dimIncrement' could be
            replaced.

            This must be called, if required, before parse() method.

            :param properties: The properties to replace in SVD
        """
        for obj in self.element.peripherals.getchildren():
            if not obj.name in properties:
                continue
            peripheral_properties = properties[obj.name]
            if 'baseAddress' in peripheral_properties:
                baseAddressEl = lxml.etree.Element('baseAddress')
                baseAddressEl.text = peripheral_properties['baseAddress']
                obj.baseAddress = baseAddressEl
            if 'dim' in peripheral_properties:
                dimEl = lxml.etree.Element('dim')
                dimEl.text = peripheral_properties['dim']
                obj.dim = dimEl
            if 'dimIncrement' in peripheral_properties:
                dimIncrementEl = lxml.etree.Element('dimIncrement')
                dimIncrementEl.text = peripheral_properties['dimIncrement']
                obj.dimIncrement = dimIncrementEl

    def append_to(self, svd):
        """
            Append this SVD to a regular SVD

            This appends this SVD (e.g peripheral definition from third party
            vendor) to a regular SVD (e.g SVD from SoC vendor).

            :param svd: The SVDDevice object t append with current SVD
        """
        for name in self.peripherals:
            svd.peripherals[name] = self.peripherals[name]
